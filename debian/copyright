Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Proc-Background
Upstream-Contact: Blair Zajac <blair@orcaware.com>
Source: https://metacpan.org/release/Proc-Background

Files: *
Copyright: 1998-2009, Blair Zajac <blair@orcaware.com>
 2019-2023, Michael Conrad
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2005, Chip Salzenberg <chip@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2008-2024, gregor herrmann <gregoa@debian.org>
 2009, Nathan Handler <nhandler@debian.org>
License: Artistic or GPL-1+
Comment: It is assumed that package maintainers have licensed their work
 under terms compatible with upstream licensing terms.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
